package Bot;

import GameServer.*;
import java.util.List;

/**
 * Created by piotr on 25/11/15.
 */
public class SnakeBot implements Controller {
    public Vector getNextMove(BoardState boardState, int currentCarId, List<Vector> positionList) {
        Vector currentPosition = boardState.getCarStateById(currentCarId).getPosition();
        Vector velocity = boardState.getCarStateById(currentCarId).getVelocity();

        Vector up = new Vector(currentPosition);
        up.addY(1);
        Vector down = new Vector(currentPosition);
        down.addY(-1);
        Vector left = new Vector(currentPosition);
        up.addX(-1);
        Vector right = new Vector(currentPosition);
        down.addX(1);

        if(boardState.getBoard().getFinish().getX() == currentPosition.getX()) {
            if(boardState.getBoard().getFinish().getY() < currentPosition.getY()) {
                return up;
            } else {
                return down;
            }
        } else {
            if(velocity.getX() == 0) {
                // We are currently moving up/down
                if(velocity.getY() > 0) {
                    return up;
                } else {
                    return down;
                }
            } else {
                // We were moving right
                if(boardState.getBoard().isEnabled(up)) {
                    return up;
                } else {
                    return down;
                }
            }
        }
    }
}
