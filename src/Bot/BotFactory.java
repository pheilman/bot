package Bot;

import GameServer.*;

/**
 * Created by piotr on 19/11/15.
 */
public class BotFactory {
    static public Controller createBot(String key) throws IllegalArgumentException {
        if(key.equals("simple")) {
            return new SimpleBot();
        } else if(key.equals("snake")) {
            return new SnakeBot();
        }
        throw new IllegalArgumentException("Uknown bot type.");
    }
}
