package Bot;

import GameServer.*;
import java.util.List;

/**
 * Created by piotr on 19/11/15.
 */
public class SimpleBot implements Controller {
    public Vector getNextMove(BoardState boardState, int currentCarId, List<Vector> positionList) {
        Vector currentPosition = boardState.getCarStateById(currentCarId).getPosition();
        Vector meta = boardState.getBoard().getFinish();

        Vector move = new Vector(currentPosition);

        if(currentPosition.getX() < meta.getX()) {
            move.addX(1);
        } else if (currentPosition.getX() > meta.getX()) {
            move.addX(-1);
        }

        if(currentPosition.getY() < meta.getY()) {
            move.addY(1);
        } else if (currentPosition.getY() > meta.getY()) {
            move.addY(-1);
        }

        return move;
    }
}
