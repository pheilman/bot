package GameServer;

import java.util.List;

/**
 * Created by piotr on 25/11/15.
 */
public interface Controller {
    public Vector getNextMove(BoardState boardState, int currentCarId, List<Vector> allowedVector);
}
