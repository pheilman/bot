package GameServer;

/**
 * Created by piotr on 25/11/15.
 */
public class Vector {
    private int x, y;

    public Vector() {
        this.x = 0;
        this.y = 0;
    }

    public Vector(Vector v) {
        this.x = v.getX();
        this.y = v.getY();
    }
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void addX(int v) {
        this.x += v;
    }

    public void addY(int v) {
        this.y += y;
    }
}
